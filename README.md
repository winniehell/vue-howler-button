# vue-howler-button

[![npm](https://img.shields.io/npm/v/vue-howler-button.svg) ![npm](https://img.shields.io/npm/dm/vue-howler-button.svg)](https://www.npmjs.com/package/vue-howler-button)
[![vue2](https://img.shields.io/badge/vue-2.x-brightgreen.svg)](https://vuejs.org/)

Vue component for playing sound using howler.js

## Table of contents

- [Installation](#installation)
- [Usage](#usage)
- [Example](#example)

# Installation

```shell
npm install --save vue-howler-button
```

or

```shell
yarn add vue-howler-button
```

## Default import

Install all the components:

```javascript
import Vue from 'vue'
import VueHowlerButton from 'vue-howler-button'

Vue.use(VueHowlerButton)
```

Use specific components:

```javascript
import Vue from 'vue'
import { SoundButton } from 'vue-howler-button'

Vue.component('sound-button', SoundButton)
```

## Distribution import

Install all the components:

```javascript
import VueHowlerButton from 'vue-howler-button/dist/vue-howler-button.common'

Vue.use(VueHowlerButton)
```

Use specific components:

```javascript
import { SoundButton } from 'vue-howler-button/dist/vue-howler-button.common'

Vue.component('sound-button', SoundButton)
```

## Browser

```html
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="vue-howler-button/dist/vue-howler-button.browser.js"></script>
```

The plugin should be auto-installed. If not, you can install it manually with the instructions below.

Install all the components:

```javascript
Vue.use(VueHowlerButton)
```

Use specific components:

```javascript
Vue.component('sound-button', VueHowlerButton.SoundButton)
```

## Source import

Install all the components:

```javascript
import Vue from 'vue'
import VueHowlerButton from 'vue-howler-button/src'

Vue.use(VueHowlerButton)
```

Use specific components:

```javascript
import Vue from 'vue'
import { SoundButton } from 'vue-howler-button/src'

Vue.component('sound-button', SoundButton)
```

**⚠️ You need to configure your bundler to compile `.vue` files.** More info [in the official documentation](https://vuejs.org/v2/guide/single-file-components.html).

# Usage

```vue
<!-- no autoplay, no loop, no styling -->
<sound-button :src="['sound.ogg']"/>

<!-- autoplay -->
<sound-button :src="['sound.ogg']" :autoplay="true"/>

<!-- autoplay -->
<sound-button :src="['sound.ogg']" :loop="true"/>

<!-- styled -->
<sound-button :src="['sound.ogg']" class="some-class"/>
```

# Example

see https://winniehell.gitlab.io/vue-howler-button/

(source: https://gitlab.com/winniehell/vue-howler-button/blob/master/public/index.html)

---

# Plugin Development

## Installation

The first time you create or clone your plugin, you need to install the default dependencies:

```
yarn install
```

## Watch and compile

This will run webpack in watching mode and output the compiled files in the `dist` folder.

```
yarn run dev
```

## Use it in another project

While developping, you can follow the install instructions of your plugin and link it into the project that uses it.

In the plugin folder:

```
yarn link
```

In the other project folder:

```
yarn link vue-howler-button
```

This will install it in the dependencies as a symlink, so that it gets any modifications made to the plugin.

## Manual build

This will build the plugin into the `dist` folder in production mode.

```
yarn run build
```

---

## License

[MIT](http://opensource.org/licenses/MIT)
